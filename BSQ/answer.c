/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   answer.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroturea <mroturea@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 15:38:38 by mroturea          #+#    #+#             */
/*   Updated: 2016/01/22 00:13:34 by dsouleym         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_answer.h"

int		ft_line_tab(int *tab, char *buf, t_tab *s)
{
	int i;
	int check;

	i = 0;
	check = 1;
	while (buf[i] != '\n')
	{
		if (buf[i] == s->empty)
			tab[i] = 1;
		else if (buf[i] == s->obstacle)
			tab[i] = 0;
		else
			return (0);
		i++;
	}
	return (check);
}

void	find_square(int **tab, int line, int col)
{
	int i;
	int j;

	i = 1;
	j = 1;
	while (i < line)
	{
		while (j < col)
		{
			if ((tab[i - 1][j - 1] <= tab[i][j - 1]) && tab[i][j] &&
					(tab[i - 1][j - 1] < tab[i - 1][j]))
				tab[i][j] = tab[i - 1][j - 1] + 1;
			else if ((tab[i][j - 1] < tab[i - 1][j - 1]) && tab[i][j] &&
					(tab[i][j - 1] < tab[i - 1][j]))
				tab[i][j] = tab[i][j - 1] + 1;
			else if (tab[i][j])
				tab[i][j] = tab[i - 1][j] + 1;
			j++;
		}
		j = 1;
		i++;
	}
}

int		answ(t_tab *s, int ans)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < s->line)
	{
		j = 0;
		while (j < s->col)
		{
			if (s->tab[i][j] == ans)
			{
				s->line_ans = i;
				s->col_ans = j;
				return (0);
			}
			j++;
		}
		i++;
	}
	return (0);
}

void	answer(t_tab *s)
{
	int i;
	int j;
	int ans;

	i = 0;
	j = 0;
	ans = 0;
	while (i < s->line)
	{
		while (j < s->col)
		{
			if (s->tab[i][j] > ans)
				ans = s->tab[i][j];
			j++;
		}
		j = 0;
		i++;
	}
	answ(s, ans);
	s->size = ans;
}

int		check_first_line(char *buf)
{
	int	i;

	i = 0;
	while (buf[i] != '\n')
		i++;
	if (buf[i - 1] != buf[i - 2] && buf[i - 2] != buf[i - 3]
			&& buf[i - 1] != buf[i - 3])
		return (0);
	else
		return (1);
}
