/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bis.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dsouleym <dsouleym@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 00:32:38 by dsouleym          #+#    #+#             */
/*   Updated: 2016/01/22 00:36:01 by dsouleym         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bis.h"

int		ft_atoi(char *str)
{
	int		i;
	int		number;

	i = 0;
	number = 0;
	while (str[i + 3] != '\n')
	{
		number *= 10;
		number += ((int)str[i] - '0');
		i++;
	}
	return (number);
}

int		get_size(char *buf)
{
	return (ft_atoi(buf));
}
