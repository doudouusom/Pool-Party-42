/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroturea <mroturea@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 15:50:46 by mroturea          #+#    #+#             */
/*   Updated: 2016/01/21 15:52:24 by mroturea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int			ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int			ft_size(char *av)
{
	int		i;
	int		fd;
	int		ret;
	char	buf[15001];
	int		k;

	fd = open(av, O_RDONLY);
	ret = read(fd, buf, 15000);
	buf[15000] = '\0';
	i = 0;
	while (buf[i] != '\n')
		i++;
	k = 0;
	i++;
	while (buf[i] != '\n' && buf[i] != '\0')
	{
		i++;
		k++;
	}
	close(fd);
	return (k);
}

int			ft_line(int size, char *av)
{
	char	buf[size + 2];
	int		fd;
	int		ret;
	int		i;

	i = 0;
	buf[size + 1] = '\0';
	fd = open(av, O_RDONLY);
	while (buf[0] != '\n')
		ret = read(fd, buf, 1);
	while ((ret = read(fd, buf, size + 1)))
	{
		i++;
		if (buf[ret - 1] != '\n')
			return (-1);
	}
	return (i);
}

void		ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void		ft_strcpy(char *dest, char *src)
{
	int		i;

	i = 0;
	while (src[i] != '\n')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
}
