/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bsq.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroturea <mroturea@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 15:54:18 by mroturea          #+#    #+#             */
/*   Updated: 2016/01/22 00:36:48 by dsouleym         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BSQ_H
# define FT_BSQ_H
# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include "struct.h"

int		ft_strlen(char *str);
int		ft_size(char *str);
int		ft_line(int size, char *str);
void	ft_putstr(char *str);
void	ft_strcpy(char *dest, char *src);
int		ft_line_tab(int *tab, char *buf, t_tab *s);
void	find_square(int	**tab, int line, int col);
void	answer(t_tab *s);
int		check_first_line(char *buf);
int		get_size(char *buf);

#endif
