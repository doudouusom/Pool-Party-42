/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroturea <mroturea@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 15:55:38 by mroturea          #+#    #+#             */
/*   Updated: 2016/01/21 16:04:02 by mroturea         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct	s_tab
{
	int		**tab;
	int		line;
	int		col;
	int		line_ans;
	int		col_ans;
	int		size;
	char	obstacle;
	char	empty;
	char	square;
	char	*name;
}				t_tab;
#endif
