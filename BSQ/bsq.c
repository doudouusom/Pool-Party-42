/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroturea <mroturea@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 15:42:22 by mroturea          #+#    #+#             */
/*   Updated: 2016/01/22 00:38:23 by dsouleym         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"

void		disp_ans(t_tab *s)
{
	int		i;
	int		j;

	j = 0;
	i = 0;
	while (i < s->line)
	{
		while (j < s->col)
		{
			if ((i <= s->line_ans && i >= s->line_ans - s->size + 1)
					&& (j <= s->col_ans && j >= s->col_ans - s->size + 1))
				write(1, &s->square, 1);
			else if (s->tab[i][j])
				write(1, &s->empty, 1);
			else
				write(1, &s->obstacle, 1);
			j++;
		}
		write(1, "\n", 1);
		j = 0;
		i++;
	}
}

int			**make_tab(t_tab *s)
{
	char	buf[s->col + 2];
	int		fd;
	int		ret;
	int		i;

	i = 0;
	buf[s->col + 2] = '\0';
	fd = open(s->name, O_RDONLY);
	s->tab = (int**)malloc(sizeof(int*) * (s->line));
	if (s->line <= 0)
		return (0);
	while (buf[0] != '\n')
		ret = read(fd, buf, 1);
	while (i < s->line)
	{
		ret = read(fd, buf, s->col + 1);
		s->tab[i] = (int*)malloc(sizeof(int) * (s->col));
		if (ft_line_tab(s->tab[i], buf, s) == 0)
			return (0);
		i++;
	}
	return (s->tab);
}

t_tab		*make_struct(char *str)
{
	t_tab	*s_tab;
	int		i;
	char	buf[215];
	int		fd;

	i = 0;
	fd = open(str, O_RDONLY);
	if (fd == -1 || read(fd, buf, 214) < 7 || check_first_line(buf))
	{
		return (0);
	}
	while (buf[i] != '\n')
		i++;
	s_tab = (t_tab*)malloc(sizeof(t_tab));
	s_tab->col = ft_size(str);
	s_tab->line = ft_line(s_tab->col, str);
	s_tab->empty = buf[i - 3];
	s_tab->obstacle = buf[i - 2];
	s_tab->square = buf[i - 1];
	s_tab->name = str;
	s_tab->tab = make_tab(s_tab);
	if (s_tab->line != get_size(buf))
		s_tab = 0;
	return (s_tab);
}

void		bsq(char *av)
{
	int		fd;
	t_tab	*s;

	fd = open("tab", O_WRONLY | O_TRUNC | O_CREAT, S_IRWXU);
	s = make_struct(av);
	if (s)
	{
		if (s->tab)
		{
			find_square(s->tab, s->line, s->col);
			answer(s);
			disp_ans(s);
		}
		else
			write(2, "map error\n", ft_strlen("map error\n"));
	}
	else
		write(2, "map error\n", ft_strlen("map error\n"));
}

void		stdinput(void)
{
	int		fd;
	int		ret;
	char	buf[151];
	t_tab	*s;

	fd = open("tab", O_WRONLY | O_TRUNC | O_CREAT, S_IRWXU);
	while ((ret = read(0, buf, 150)))
	{
		write(fd, buf, ret);
	}
	s = make_struct("tab");
	if (s)
	{
		if (s->tab)
		{
			find_square(s->tab, s->line, s->col);
			answer(s);
			disp_ans(s);
		}
		else
			write(2, "map error\n", ft_strlen("map error\n"));
	}
	else
		write(2, "map error\n", ft_strlen("map error\n"));
}
